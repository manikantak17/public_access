function FindProxyForURL(url, host) {
    if (shExpMatch(url, "*.devcloudstation.io*") || shExpMatch(url, "*.cloudstation.io*")) {
        return "PROXY 27.59.151.152:2222";
    } else {
        return "DIRECT";
    }
}